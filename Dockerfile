FROM php:7.4.20-apache

RUN apt-get update -y && \
    apt-get upgrade -y && \
    apt-get install -y apt-utils zip unzip curl && \
    apt-get install -y nano vim git && \
    apt-get install -y libzip-dev cron libpng-dev && \
    a2enmod rewrite && \
    docker-php-ext-install pdo_mysql bcmath mysqli &&\
    docker-php-ext-configure zip &&\
    docker-php-ext-install zip exif &&\
    apt-get install -y \
    libwebp-dev \
    libjpeg62-turbo-dev \
    libpng-dev libxpm-dev \
    libfreetype6-dev


RUN docker-php-ext-configure gd\
    --with-webp \
    --with-jpeg \
    --with-xpm \
    --with-freetype && \
    docker-php-ext-install -j$(nproc) gd && \
    docker-php-ext-enable gd && \
    rm -rf /tmp/*

WORKDIR /var/www/html



#composer

RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN php composer-setup.php
RUN php -r "unlink('composer-setup.php');"

COPY . /var/www/html

RUN php composer.phar install

#chown -R www-data:www-data /var/www/html/storage /var/www/html/bootstrap/cache /var/www/html/public
RUN chown -R www-data:www-data /var/www/html/
RUN chmod -R 755 /var/www/html/storage

EXPOSE 80

RUN a2enmod rewrite headers

COPY start.sh /usr/local/bin/start.sh
RUN chmod -R 755 /usr/local/bin/start.sh
COPY default.conf /etc/apache2/sites-available/000-default.conf

CMD ["/usr/local/bin/start.sh"]


